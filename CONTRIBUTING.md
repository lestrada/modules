# Contributing

To contribute to this repository, you need to [fork it](https://gitlab.biologie.ens-lyon.fr/PSMN/modules/forks/new).

## Development on the PSMN

To install your forked version of the LBMC modules repository on the PSMN run following command:

```
git clone git@gitlab.biologie.ens-lyon.fr:<your_login>/modules.git ~/privatemodules_dev
module use ~/privatemodules_dev
```

If you don’t have a `.ssh/config` file and if you like typing your password, you can instead use the following clone command, after defining your [Gitlab password](https://gitlab.biologie.ens-lyon.fr/profile/password/edit).

```sh
git clone -c http.sslVerify=false https://gitlab.biologie.ens-lyon.fr:<your_login>/modules.git ~/privatemodules_dev
```

You have to run the `/apps/<software_name>/install.sh` script to install
the software required by the module you want to use.

### New version of an existing module

To propose a new version of a module, start by running the following commands:

```sh
cd ~/privatemodules_dev/apps/<software_name>/
cp install_<existing_version>.sh install_<new_version>.sh
```

Then edit the `install_<new_version>.sh` file. This file must be executable
without error on the PSMN to create a proper installation of the software in
the folder ``/apps/<software_name>/<version>`.

You must add a line `apps/<software_name>/<version>` in the `.gitignore` file,
otherwise the installation files could be committed to your repository.

Then, create the corresponding module file with the following commands:

```sh
cd ~/privatemodules_dev/modulefiles/<software_name>/
cp <existing_version>.lua <new_version>.lua
sed -i `s/<existing_version>/<new_version>/g` <new_version>.lua
```

### New module

To propose a new module, you can use an existing module as a template to write your `apps/<software>/<version>_install.sh` script and `modulefiles/<software>/<version>.lua` file.

You can look at the following modules to have examples of :

- Python tools : [MultiQC install.sh](https://gitlab.biologie.ens-lyon.fr/PSMN/modules/blob/master/apps/MultiQC/install_1.0.sh) and [MultiQC .lua](https://gitlab.biologie.ens-lyon.fr/PSMN/modules/blob/master/modulefiles/MultiQC/1.0.lua)
- Compiled tools : [UrQt install.sh](https://gitlab.biologie.ens-lyon.fr/PSMN/modules/blob/master/apps/UrQt/install_d62c1f8.sh)
- Java tools : [Trimmomatic install.sh](https://gitlab.biologie.ens-lyon.fr/PSMN/modules/blob/master/apps/Trimmomatic/install_0.36.sh) and [nextflow.lua](https://gitlab.biologie.ens-lyon.fr/PSMN/modules/blob/master/modulefiles/nextflow/0.28.2.lua)

## Making your module available to the LBMC

To make your module available to the LBMC you must have a `install_<new_version>.sh` script working without errors on the E5 compilation servers and you must be able to load the correspond module and run it with the commands:

```sh
module use ~/privatemodules_dev
ml <module_name>/<module_version>
```

After pushing your modifications to your forked repository, you can make a Merge Request to the [PSMN/modules](https://gitlab.biologie.ens-lyon.fr/PSMN/modules) **dev** branch. Where it will be tested and 
integrated to the **master** branch.

You can read more on this process [here](https://guides.github.com/introduction/flow/)

## Migrate old module to the new module system

To convert an old module to the new module system you can run the following
commands:

```sh
ml load Core/lmod
tcl2lua.tcl data/common/modules/modulefiles/Bowtie2/2.3.2
```

This command will display the new module file for your module.
